package connectedsubgraph;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.HashSet;
import java.util.Set;

/**
 * MIP implements a mixed integer linear programming model for the connected
 * subgraph problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {
  private static final double HALF = 0.5;  // for rounding integer variables
  private final IloCplex mip;        // the MIP model
  private final IloNumVar[] chosen;  // indicators for selected nodes
  private final IloNumVar[] source;  // indicators for designated source node
  private final IloNumVar[] flow;    // flow variables (indexed by arc)

  /**
   * Constructor.
   * @param graph the graph to solve
   * @param m the size of the required subgraph
   * @throws IloException if the model cannot be built
   */
  public MIP(final Graph graph, final int m) throws IloException {
    // Get the collection of nodes.
    int n = graph.getnNodes();
    // Initialize the model.
    mip = new IloCplex();
    // Create the variables.
    chosen = new IloNumVar[n];
    source = new IloNumVar[n];
    int nArcs = graph.getnArcs();
    flow = new IloNumVar[nArcs];
    for (int i = 0; i < n; i++) {
      chosen[i] = mip.boolVar("chosen_" + i);
      source[i] = mip.boolVar("source_" + i);
    }
    for (int k = 0; k < nArcs; k++) {
      int[] a = graph.getArc(k);
      flow[k] = mip.numVar(0, m, "flow_" + a[0] + "_" + a[1]);
    }
    // Since this is a feasibility problem, we use the default objective value
    // (minimize 0).
    // Constraint: The correct number of nodes must be chosen.
    mip.addEq(mip.sum(chosen), m, "subgraph_size");
    // Constraint: One node must be designated the source.
    mip.addEq(mip.sum(source), 1.0, "single_source");
    // Constraint: The source node must be chosen.
    for (int i = 0; i < n; i++) {
      mip.addLe(source[i], chosen[i], "choose_source_" + i);
    }
    // Constraint: The flow into or out of a node that is not chosen must be 0.
    for (int i = 0; i < n; i++) {
      for (int k : graph.inboundAt(i)) {
        mip.addLe(flow[k], mip.prod(m, chosen[i]),
                  "flow_choice_in_" + k + "_" + i);
      }
      for (int k : graph.outboundAt(i)) {
        mip.addLe(flow[k], mip.prod(m, chosen[i]),
                  "flow_choice_out_" + k + "_" + i);
      }
    }
    // Constraint: The net flow out of a node (flow out - flow in) must be
    // m - 1 for the source node, -1 for every other selected node and 0
    // otherwise.
    for (int i = 0; i < n; i++) {
      IloLinearNumExpr netFlow = mip.linearNumExpr();
      for (int k : graph.outboundAt(i)) {
        netFlow.addTerm(1.0, flow[k]);
      }
      for (int k : graph.inboundAt(i)) {
        netFlow.addTerm(-1.0, flow[k]);
      }
      mip.addEq(netFlow, mip.diff(mip.prod(m, source[i]), chosen[i]),
                "net_flow_" + i);
    }
  }

  /**
   * Tries to solve the model.
   * @return true if a connected subgraph of the correct size is found,
   * false if none exists.
   * @throws IloException if CPLEX throws an error
   */
  public boolean solve() throws IloException {
    mip.solve();
    IloCplex.Status status = mip.getStatus();
    if (status == IloCplex.Status.Optimal) {
      return true;
    } else if (status == IloCplex.Status.Infeasible) {
      return false;
    } else {
      throw new IloException("Unexpected status: " + status);
    }
  }

  /**
   * Gets the subgraph found by the solver.
   * @return the set of nodes in the subgraph
   * @throws IloException if no subgraph exists or CPLEX cannot produce the
   * solution
   */
  public Set<Integer> getSubgraph() throws IloException {
    if (mip.getStatus() == IloCplex.Status.Optimal) {
      HashSet<Integer> nodes = new HashSet<>();
      double[] vals = mip.getValues(chosen);
      for (int i = 0; i < vals.length; i++) {
        if (vals[i] > HALF) {
          nodes.add(i);
        }
      }
      return nodes;
    } else {
      throw new IloException("No subgraph found.");
    }
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    mip.close();
  }

}
