# Finding a Connected Subgraph

This repository contains a Java program that compares a mixed integer programming model (solved with CPLEX) and a simple enumeration scheme (building a layered subgraph node by node) to solve a [problem](https://math.stackexchange.com/questions/4693636/milp-constraints-for-connectivity-in-a-subgraph) posted on Mathematics Stack Exchange. The issue is to find a connected subgraph of a given graph, such that the subgraph has a specified number of nodes (or prove no such subgraph exists).

The Math SE question asks about how to impose constraints in a MIP model to enforce the connectedness requirement. The code here demonstrates how to use flows to do so, but also shows a simple constructive technique which consumes less memory and is faster for large graphs.

The program requires CPLEX (tested with version 22.1.1) but is otherwise self-contained. In addition to running a couple of simple examples from the original post (one feasible, one not), it generates a larger random test graph and solves that.

The MIP model and constructive method are described in a [blog post](https://orinanobworld.blogspot.com/2023/05/finding-connected-subgraph.html).
