package connectedsubgraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Graph is a container for an undirected graph.
 *
 * Nodes are indexed 0, 1, ..., N-1.
 *
 * To support the flow-based MIP model, each edge is treated as a pair of
 * arcs, one in each direction.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Graph {
  // Column indices for the arc matrix.
  private static final int TAIL = 0;
  private static final int HEAD = 1;

  private final HashSet<Integer> nodes;  // the set of nodes
  private final HashMap<Integer, HashSet<Integer>> adjacent;
    // maps nodes to adjacent nodes
  private int[][] arcs;  // arc equivalents of edges
  private final HashMap<Integer, HashSet<Integer>> outbound;
    // maps nodes to indices of outbound arcs
  private final HashMap<Integer, HashSet<Integer>> inbound;
    // maps nodes to indices of inbound arcs

  /**
   * Constructs a graph from an adjacency matrix.
   * @param adjacencyMatrix a 0-1 adjacency matrix defining the graph
   */
  public Graph(final int[][] adjacencyMatrix) {
    int n = adjacencyMatrix[0].length;  // node count
    nodes = new HashSet<>();
    nodes.addAll(IntStream.range(0, n).boxed().toList());
    adjacent = new HashMap<>();
    outbound = new HashMap<>();
    inbound = new HashMap<>();
    // Create the adjacency map.
    for (int i = 0; i < n; i++) {
      HashSet<Integer> linked = new HashSet<>();
      for (int j = 0; j < n; j++) {
        if (adjacencyMatrix[i][j] == 1) {
          linked.add(j);
        }
      }
      // Nodes should not be considered adjacent to themselves.
      linked.remove(i);
      adjacent.put(i, linked);
    }
    // Set up the inbound and outbound maps.
    for (int i = 0; i < n; i++) {
      inbound.put(i, new HashSet<>());
      outbound.put(i, new HashSet<>());
    }
    // Convert the arc list to an array and populate the inbound/outbound maps.
    buildArcMatrix();
  }

  /**
   * Generates a random graph instance.
   * @param nodeCount the number of nodes
   * @param density the edge density (between 0 and 1)
   * @param seed a seed for a random number generator
   */
  public Graph(final int nodeCount, final double density, final long seed) {
    // Generate the specified number of nodes.
    nodes = new HashSet<>(IntStream.range(0, nodeCount).boxed().toList());
    // Initialize the adjacency map.
    adjacent = new HashMap<>();
    for (int n = 0; n < nodeCount; n++) {
      adjacent.put(n, new HashSet<>());
    }
    // Set up a random number generator.
    Random rng = new Random(seed);
    // For each pair of nodes, randomly decide if an edge connects them.
    for (int n = 0; n < nodeCount; n++) {
      for (int m = n + 1; m < nodeCount; m++) {
        if (rng.nextDouble() < density) {
          adjacent.get(n).add(m);
          adjacent.get(m).add(n);
        }
      }
    }
    // Set up the inbound and outbound maps.
    outbound = new HashMap<>();
    inbound = new HashMap<>();
    for (int i = 0; i < nodeCount; i++) {
      inbound.put(i, new HashSet<>());
      outbound.put(i, new HashSet<>());
    }
    // Convert the arc list to an array and populate the inbound/outbound maps.
    buildArcMatrix();
  }

  /**
   * Gets the set of nodes in the graph.
   * @return the node set
   */
  public Collection<Integer> getAllNodes() {
    return Collections.unmodifiableCollection(nodes);
  }

  /**
   * Gets the number of nodes.
   * @return the node count
   */
  public int getnNodes() {
    return nodes.size();
  }

  /**
   * Gets the collection of nodes adjacent to a given node.
   * @param n the given node
   * @return the collection of adjacent nodes
   */
  public Collection<Integer> adjacentTo(final int n) {
    return Collections.unmodifiableCollection(adjacent.get(n));
  }

  /**
   * Gets the collection of all nodes *not* adjacent to a given node
   * (including the given node itself).
   * @param n the given node
   * @return the collection of nodes not adjacent to it
   */
  public Collection<Integer> notAdjacentTo(final int n) {
    Collection<Integer> c = new HashSet<>(nodes);
    c.removeAll(adjacent.get(n));
    return c;
  }

  /**
   * Gets the collection of all nodes indexed above a threshold (inclusive)
   * and adjacent to a given node.
   * @param n the given node
   * @param first the lowest index to include
   * @return the collection of adjacent nodes with qualifying indices
   */
  public Collection<Integer> adjacentTo(final int n, final int first) {
    HashSet<Integer> s =
      new HashSet<>(adjacent.get(n).stream().filter(x -> x >= first).toList());
    return s;
  }

  /**
   * Builds the arc matrix and the inbound/outbound arc maps from the adjacency
   * information.
   */
  private void buildArcMatrix() {
    // Declare a local record class for arcs.
    record Arc(int tail, int head) { }
    // Create a list of arcs.
    ArrayList<Arc> arcList = new ArrayList<>();
    for (int n : nodes) {
      for (int m : adjacent.get(n)) {
        arcList.add(new Arc(n, m));
      }
    }
    // Convert the arc list to an array and populate the inbound/outbound maps.
    arcs = new int[arcList.size()][2];
    for (int k = 0; k < arcList.size(); k++) {
      Arc a = arcList.get(k);
      int t = a.tail();
      int h = a.head();
      arcs[k][TAIL] = t;
      arcs[k][HEAD] = h;
      inbound.get(h).add(k);
      outbound.get(t).add(k);
    }
  }

  /**
   * Gets the number of arcs.
   * @return the arc count
   */
  public int getnArcs() {
    return arcs.length;
  }

  /**
   * Gets a single arc.
   * @param k the index of the arc
   * @return the arc as a vector
   */
  public int[] getArc(final int k) {
    return arcs[k];
  }

  /**
   * Returns the indices of arcs inbound at a given node.
   * @param n the node
   * @return the indices of inbound arcs
   */
  public Collection<Integer> inboundAt(final int n) {
    return Collections.unmodifiableCollection(inbound.get(n));
  }

  /**
   * Returns the indices of arcs outbound at a given node.
   * @param n the node
   * @return the indices of outbound arcs
   */
  public Collection<Integer> outboundAt(final int n) {
    return Collections.unmodifiableCollection(outbound.get(n));
  }

  /**
   * Produces a string describing the graph.
   * @return a description of the graph.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("The graph contains ");
    sb.append(nodes.size()).append(" nodes and ").append(arcs.length / 2)
      .append(" edges.");
    return sb.toString();
  }
}
