package connectedsubgraph;

import ilog.concert.IloException;
import java.util.Optional;
import java.util.Set;

/**
 * ConnectedSubgraph tests MIP and brute-force approaches to finding a
 * connected subgraph with a specified number of vertices in an undirected
 * graph (or proving that none exists).
 *
 * The motivation is a question on Mathematics Stack Exchange:
 * https://math.stackexchange.com/questions/4693636/milp-constraints-for
 * -connectivity-in-a-subgraph
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class ConnectedSubgraph {

  /** Dummy constructor. */
  private ConnectedSubgraph() { }

  /**
   * Runs the computational tests.
   * @param args the command line arguments ignored
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {

    // Create the test graph from the Math SE post.
    int[][] aMatrix = new int[][] {{1, 1, 0, 1, 0, 0},
                                   {1, 1, 1, 1, 0, 0},
                                   {0, 1, 1, 1, 0, 0},
                                   {1, 1, 1, 1, 0, 0},
                                   {0, 0, 0, 0, 1, 1},
                                   {0, 0, 0, 0, 1, 1}};
    Graph graph = new Graph(aMatrix);

    // Try finding a connected graph of size 4 using the MIP model.
    int size = 4;
    System.out.println("\nLooking for subgraph size " + size + " via MIP.");
    try (MIP mip = new MIP(graph, size)) {
      if (mip.solve()) {
        System.out.println("A connected subgraph of size "
                           + size + " exists:");
        System.out.println(mip.getSubgraph());
      } else {
        System.out.println("No connected subgraph of size "
                            + size + " exists.");
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }

    // Now try the layered search.
    System.out.println("\nLooking for subgraph size " + size + " via search.");
    Optional<Set<Integer>> subgraph = LayeredSearch.findSubgraph(graph, size);
    if (subgraph.isPresent()) {
      System.out.println("A connected subgraph of size " + size + " exists:");
      System.out.println(subgraph.get().stream().sorted().toList());
    } else {
        System.out.println("No connected subgraph of size "
                           + size + " exists.");
    }

    // Try the MIP model with a target of 5 nodes (unattainable).
    size = 5;
    System.out.println("\nLooking for subgraph size " + size + " via MIP.");
    try (MIP mip = new MIP(graph, size)) {
      if (mip.solve()) {
        System.out.println("A connected subgraph of size "
                           + size + " exists:");
        System.out.println(mip.getSubgraph());
      } else {
        System.out.println("No connected subgraph of size "
                            + size + " exists.");
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }

    // Now try the layered search.
    System.out.println("\nLooking for subgraph size " + size + " via search.");
    subgraph = LayeredSearch.findSubgraph(graph, size);
    if (subgraph.isPresent()) {
      System.out.println("A connected subgraph of size " + size + " exists:");
      System.out.println(subgraph.get().stream().sorted().toList());
    } else {
        System.out.println("No connected subgraph of size "
                           + size + " exists.");
    }

    // Create a random graph.
    int nodeCount = 5000;    // number of nodes in the graph
    double density = 0.05;   // target fraction of possible edges that exist
    long seed = 123;         // random number seed
    size = 1000;             // target size for the subgraph
    graph = new Graph(nodeCount, density, seed);
    System.out.println("\nRandom graph:\n" + graph);

    // Try the MIP model.
    System.out.println("\nLooking for subgraph size " + size + " via MIP.");
    long clock = System.currentTimeMillis();
    try (MIP mip = new MIP(graph, size)) {
      if (mip.solve()) {
        System.out.println("A connected subgraph of size "
                           + size + " exists:");
        System.out.println(mip.getSubgraph());
      } else {
        System.out.println("No connected subgraph of size "
                            + size + " exists.");
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    double time = (System.currentTimeMillis() - clock) / 1000;
    System.out.println("Round trip time = " + String.format("%.2f", time)
                       + " seconds.");

    // Try the layered search.
    System.out.println("\nLooking for subgraph size " + size + " via search.");
    clock = System.currentTimeMillis();
    subgraph = LayeredSearch.findSubgraph(graph, size);
    if (subgraph.isPresent()) {
      System.out.println("A connected subgraph of size " + size + " exists:");
      System.out.println(subgraph.get().stream().sorted().toList());
    } else {
        System.out.println("No connected subgraph of size "
                           + size + " exists.");
    }
    time = System.currentTimeMillis() - clock;
    System.out.println("Round trip time = " + String.format("%.2f", time)
                       + " ms.");
  }
}
