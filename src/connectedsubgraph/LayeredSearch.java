package connectedsubgraph;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

/**
 * LayeredSearch uses a layered graph construction approach to find a connected
 * subgraph of desired size (or prove none exists).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class LayeredSearch {

  /**
   * Dummy constructor.
   */
  private LayeredSearch() { }

  /**
   * Attempts to find a connected subgraph of desired size.
   * @param graph the source graph
   * @param size the target subgraph size
   * @return the set of nodes in the subgraph if one is found
   */
  public static Optional<Set<Integer>>
    findSubgraph(final Graph graph, final int size) {
    // Sequentially try "root" nodes until a subgraph is found or the
    // possibilities are exhausted.
    for (int first = 0; first < graph.getnNodes() - size + 1; first++) {
      Optional<Set<Integer>> g = findSubgraph(graph, size, first);
      if (g.isPresent()) {
        return g;
      }
    }
    // If we fall through to here, the problem is infeasible.
    return Optional.empty();
  }

  /**
   * Attempts to find a connected subgraph of desired size using only nodes
   * with indices above a starting value (inclusive).
   * @param graph the source graph
   * @param size the target subgraph size
   * @param first the lowest node index to consider
   * @return the set of nodes in the subgraph if one is found
   */
  private static Optional<Set<Integer>>
    findSubgraph(final Graph graph, final int size, final int first) {
      // Start with the lowest index node as the "root" of the subgraph.
      HashSet<Integer> sub = new HashSet<>();
      sub.add(first);
      // Get the qualifying nodes adjacent to the root.
      Collection<Integer> adjacent = graph.adjacentTo(first, first);
      // If the subgraph can be built with depth 1, do so.
      if (adjacent.size() >= size - 1) {
        sub.addAll(trim(adjacent, size - 1));
        return Optional.of(sub);
      }
      // Add all the adjancent nodes to the subgraph and queue them for
      // processing.
      sub.addAll(adjacent);
      Queue<Integer> todo = new LinkedList<>(adjacent);
      int togo = size - sub.size();  // number of nodes needed
      while (!todo.isEmpty()) {
        // Pop the next node off the queue.
        int node = todo.poll();
        // Find qualifying nodes adjacent to this node and not already in
        // the subgraph.
        adjacent = graph.adjacentTo(node, first);
        adjacent.removeAll(sub);
        // If enough nodes are left to finish the subgraph, do so.
        if (adjacent.size() >= togo) {
          sub.addAll(trim(adjacent, togo));
          return Optional.of(sub);
        }
        // Add all the adjancent nodes to the subgraph and queue them for
        // processing.
        sub.addAll(adjacent);
        todo.addAll(adjacent);
        togo = size - sub.size();
      }
      // If we fall through to here, it is not possible to build the subgraph
      // with the specified root.
      return Optional.empty();
    }

    /**
     * Extracts an arbitrary subset of given size from a collection.
     * @param source the source collection
     * @param size the target size
     * @return a collection of the desired size
     */
    private static Collection<Integer>
      trim(final Collection<Integer> source, final int size) {
        return source.stream().limit(size).toList();
    }
}
